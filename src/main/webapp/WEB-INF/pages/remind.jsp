<!DOCTYPE html>
<html>
<head>
    <META content="text/html" charset="utf-8" http-equiv="Content-Type">
    <title>Вход</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        body {
            padding-top: 40px;
            padding-bottom: 40px;
        }

        button {
            left: 730;
        }

        input[type="text"] {
            font-size: 16px;
            height: auto;
            margin-bottom: 15px;
            padding: 7px 9px;
        }

    </style>

</head>
<body>
<form method="POST" action="/remind">
    <div class="col-lg-offset-3">
        <label>${lable}</label>
        <h3>${question}</h3>
        <p>Write answer here:</p>
        <input type="text" name="answer" placeholder="Answer"><br>
        <input class="btn-primary btn" type="submit" value="Go">
    </div>
</form>
</body>