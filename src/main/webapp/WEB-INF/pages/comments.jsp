<!DOCTYPE html>
<html>
<head>
    <META content="text/html; charset=utf-8" http-equiv="Content-Type">
    <title>Вход</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        body {
            padding-top: 40px;
            padding-bottom: 40px;
            background-image: url("http://hq-wallpapers.ru/wallpapers/13/hq-wallpapers_ru_nature_61651_1920x1200.jpg");
        }

        button {
            left: 730;
        }

        .form-signin {
            max-width: 300px;
            padding: 19px 29px 29px;
            margin: 0 auto 20px;
            background-color: #fff;
            border: 1px solid #e5e5e5;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
            -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
            box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
        }

        .form-signin input[type="text"],
        .form-signin input[type="password"] {
            font-size: 16px;
            height: auto;
            margin-bottom: 15px;
            padding: 7px 9px;
        }

    </style>

</head>
<body>
<div class="col-lg-offset-3">
    <a href="/hello">
        <button class="btn-link" style="background-color: #ffffff">Back</button>
    </a>

    <h2>
        Comments:
    </h2>
    ${comments}
    <br>
    <div class="col-xs-12" style="padding: 50px"><br><br>
    <form action="/add_comment" method="post">
        <h3>Add comment</h3>
        <input hidden="hidden" name="id" value="${id}">
        <textarea name="comment"></textarea><br>
        <button formmethod="post" class="btn-primary btn" type="submit">Send!</button>
    </form></div>
</div>

<form style="padding: 19%" action="/logout" method="post">
    <button class="btn-danger" type="submit">Logout</button>
</form>
</body>
</html>
