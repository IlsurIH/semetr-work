<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <META content="text/html" charset="utf-8" http-equiv="Content-Type">
    <title>Моя страница</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        body {
            padding-top: 40px;
            padding-bottom: 40px;
            /*background-image: url("http://www.o-prirode.com/_ph/28/661099116.jpg");*/
        }

        button {
            left: 730px;
        }

        .form-posts {
            max-width: 300px;
            padding: 19px 29px 29px;
            margin: 0 auto 20px;
            background-color: #fff;
            border: 1px solid #e5e5e5;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
            -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
            box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
        }

        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 10px;
        }

        .form-signin input[type="text"],
        .form-signin input[type="password"] {
            font-size: 16px;
            height: auto;
            margin-bottom: 15px;
            padding: 7px 9px;
        }

    </style>

</head>
<body>
<h1 style="color: #28a4c9;text-shadow: #464646 0px 1px 0" align="center">Привет, <%=session.getAttribute("login")%></h1>

<form class="form-posts" action="hello" align="center" method="post">
    ${addPostError}

    <h2 style="color: #28a4c9">Добавить пост</h2>
    <label style="color: #28a4c9" for="title">Описание</label><br>
    <textarea style="resize: none" id="title" cols="30" name="title" type="text"></textarea>
    <label style="color: #28a4c9" for="post">Ну и сам пост</label><br>
    <textarea id=post name="post" rows="10" cols="30"></textarea>
    <br>
    <input type="image" src="../resources/post.jpg">
</form>
<%=session.getAttribute("posts")%>

<form style="padding: 19%" action="/logout" method="post">
    <button class="btn-danger" type="submit">Logout</button>
</form>

</body>
</html>