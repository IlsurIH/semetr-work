package com.springapp.mvc;


import database.LocalhostDBConnection;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    HttpSession session;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String welcomePage(HttpServletRequest request) {
        session = request.getSession();
        return authorisedRedirect("redirect:/hello");
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String printWelcome(@RequestParam(defaultValue = "<no>") String login,
                               @RequestParam(defaultValue = "<no>") String password,
                               ModelMap model) {
        if (session.getAttribute("login") != null) {
            return "redirect:/hello";
        } else {
            if (DatabaseWorker.Registrated(login, password)) {
                session.setAttribute("login", login);
                System.out.println(session.getAttribute("login"));
                return "redirect:/hello";
            } else {
                model.addAttribute("login_error", "<p><font size=\"3\" color=\"red\" " +
                        "face=\"Arial\">Wrong username/password</font><br>");
                System.out.println("Tried to login as:\n~" + login + " \n~" + password
                        + " \n~" + DatabaseWorker.Registrated(login, password));
                return "login";
            }
        }

    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String printHello() {
        return "login";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registrationPage() {
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String regUser(@RequestParam(defaultValue = "<no>") String login,
                          @RequestParam(defaultValue = "<no>") String password,
                          @RequestParam(defaultValue = "<no>") String password_2,
                          @RequestParam(defaultValue = "<no>") String question,
                          @RequestParam(defaultValue = "<no>") String question2,
                          ModelMap model) {
        if (!DatabaseWorker.Registrated(login, password) && password.equals(password_2)) {
            System.out.print("Adding to base:" +
                    login + ", " + password);
            LocalhostDBConnection.registrate(login, password, question, question2);
            session.setAttribute("login", login);
            return "redirect:/hello";
        } else
            model.addAttribute("registration_error", "<p><font size=\"3\" color=\"red\" +\"face=\"Arial\">Username is already used, try another</font><br>");
        return "registration";
    }


    String authorisedRedirect(String returner) {
        if (session.getAttribute("login") == null)
            return "redirect:/login";
        else
            return returner;
    }

    static String user_login;

    @RequestMapping(value = "/forgot", method = RequestMethod.GET)
    public String forgot() {
        return "forgot";
    }
    static String question;

    @RequestMapping(value ="/forgot", method = RequestMethod.POST)
    public String red(@RequestParam String login, ModelMap map){
        user_login = login;
        question = LocalhostDBConnection.getQuestion(user_login);
        if(question == null){
            map.addAttribute("error", "Wrong login");
            return "forgot";
        }
        else return "redirect:/remind";
    }
    @RequestMapping(value = "/remind", method = RequestMethod.GET)
    public String reminder(ModelMap map) {
        map.addAttribute("question", question);
        return "remind";
    }

    @RequestMapping(value = "/remind", method = RequestMethod.POST)
    public String reminder(@RequestParam String answer, ModelMap map) {
        String password = LocalhostDBConnection.isTrue(user_login, answer);
        if (password != null) {
            map.addAttribute("login_error", "password is <b>"+password+"</b>");
            return "login";
        } else {
            map.addAttribute("question", question);
            map.addAttribute("lable", "Wrong answer, think one more time:");
            return "remind";
        }
    }

}
