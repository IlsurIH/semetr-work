package com.springapp.mvc;

import database.LocalhostDBConnection;

import java.sql.Connection;
import java.sql.SQLException;

public class DatabaseWorker{


    public static boolean addPost(String post, String author, String Date, String title){
        return LocalhostDBConnection.addPost(post, author, Date, title);}

    public  static boolean Registrated(String login, String password){
        return LocalhostDBConnection.registrated(login, password);
    }

    public static boolean deletePost(int id){
        return LocalhostDBConnection.deletePost(id);
    }

    public static boolean likedPost(int id) {
        return LocalhostDBConnection.likedPost(id, "true");
    }
}
