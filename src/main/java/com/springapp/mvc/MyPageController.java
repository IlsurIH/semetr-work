package com.springapp.mvc;


import database.LocalhostDBConnection;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;

@Controller
public class MyPageController {

    static HttpSession session;
    static String login;

    @RequestMapping(value = "/hello*", method = {RequestMethod.POST})
    public String myPage(@RequestParam String post,
                         @RequestParam String title, ModelMap model) {
        System.out.println("POST");
        if (!LocalhostDBConnection.addPost(post, login, String.valueOf(new java.sql.Date((new Date()).getTime())), title))
            model.addAttribute("addPostError", "<h1>Unable to add post</h1>");

        String posts = LocalhostDBConnection.getPosts(login);
        System.out.println("Posts loaded");
        session.setAttribute("posts", posts);

        return "hello";
    }


    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String Page(HttpServletRequest req, ModelMap map) {
        session = req.getSession();
        login = (String) session.getAttribute("login");
        session.setAttribute("posts", LocalhostDBConnection.getPosts(login));
        return "hello";
    }

    @RequestMapping(value = "/delete_post", method = RequestMethod.POST)
    public String deletePost(@RequestParam int id, ModelMap map) {
        if (LocalhostDBConnection.deletePost(id)) {
            String posts = LocalhostDBConnection.getPosts(login);
            session.setAttribute("posts", posts);
            return "redirect:/hello";
        } else return "error_page";
    }

    static int postID = -1;
    @RequestMapping(value = "/comments")
    public String comments(@RequestParam int id, ModelMap map) {
        postID = id;
        map.addAttribute("comments", LocalhostDBConnection.getComments(id, login));
        map.addAttribute("id", id);
        return "comments";
    }

    @RequestMapping(value = "/like", method = RequestMethod.GET)
    public String like(@RequestParam int id, ModelMap map) {
        if (LocalhostDBConnection.likedPost(id, login)) {
            String posts = LocalhostDBConnection.getPosts(login);
            session.setAttribute("posts", posts);
            return "redirect:/hello";
        }
        else{
            return "error";
        }
    }

    @RequestMapping(value = "/add_comment")
    public String comment(@RequestParam String comment, @RequestParam int id){
        LocalhostDBConnection.addComment(comment, login, id);
        return ("redirect:/comments?id="+postID);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String logout(){
        LocalhostDBConnection.close();
        session.setAttribute("login", null);
        return "redirect:/login";
    }

    @RequestMapping(value="/delete_comment", method = RequestMethod.POST)
    public String deleteComm(@RequestParam int id){
        if (LocalhostDBConnection.deleteComment(id)) {
            String comments = LocalhostDBConnection.getComments(postID, login);
            session.setAttribute("comments", comments);
            return ("redirect:/comments?id="+postID);
        } else return "error_page";
    }

    @RequestMapping(value = "/like_comment", method = RequestMethod.POST)
    public String likeComment(@RequestParam int id) {
        if (LocalhostDBConnection.likedComment(id, login)) {
            String comments = LocalhostDBConnection.getComments(postID, login);
            session.setAttribute("comments", comments);
            return ("redirect:/comments?id="+postID);
        }
        else{
            return "error";
        }
    }

}
