package database;

import java.sql.PreparedStatement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

public class LocalhostDBConnection {
    private static Connection connection = null;

    static Connection getConnection() {
        if (connection == null) {
            try {
                // Название драйвера
                String driverName = "com.mysql.jdbc.Driver";
                Class.forName(driverName);
                // Create a connection to the database
                String serverName = "localhost";
                String mydatabase = "sot_netw";
                String url = "jdbc:mysql://" + serverName + "/" + mydatabase;
                String username = "root";
                String password = "";
                connection = DriverManager.getConnection(url, username, password);
                System.out.println("is connect to DB" + connection);
            } // end try
            catch (ClassNotFoundException e) {
                e.printStackTrace();
                // Could not find the database driver
            } catch (SQLException e) {
                e.printStackTrace();
                // Could not connect to the database
            }
        }
        return connection;

    }

    public static String getPosts(String login) {
        Connection conc = getConnection();
        String returner = "<h1 style='color: red' >ERROR with DATABASE</h1>";
        try {
            String query = "Select * FROM posts";
            Statement stmt = conc.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            returner = "<h1>Posts:</h1><br>";
            while (resultSet.next()) {
                boolean hasPower = false;
                int id = resultSet.getInt("postID");
                String TITLE = resultSet.getString("title");
                String POST = resultSet.getString("post");
                String TIME = resultSet.getString("time");
                String author = resultSet.getString("author");

                if (author.equals(login))
                    hasPower = true;
                String part1 = "<div class=\"post\"><div class=\"col-lg-2\"><h3>" + resultSet.getString("title") + "</h3>\n" +
                        "            <p>" + resultSet.getString("post") + "</p></div><br><br><br><div>\n";
                ///
                String p1 = "<div class=\"post\">\n" +
                        "    <div class=\"col-lg-12\">\n" +
                        "        <h3 style=\"float: left\">"+TITLE+"</h3>";
                ///

                String button =
                        "        <form action=\"/delete_post?id=" + id + "\" method = post>" +
                                "        <button>x</button></form>";
                ///
                String btn = "  <form align=\"right\" method='post' action=\"/delete_post?id="+id+"\">\n" +
                        "            <button style=\"margin-top: 15px;\" type=\"submit\">x</button>\n" +
                        "       </form>";
                ///
                String part2 =
                        "        </div>\n" +
                                "        <br>\n" +
                                "\n" +
                                "        <div class=\"col-lg-2\">\n" +
                                "            <br>" +
                                "            <p><b>" + author + ":</b> <i>" + resultSet.getString("time") + "</i></p>\n" +
                                "        </div>\n" +
                                "        <br>\n" +
                                "        <br>\n" +
                                "        <div class=\"col-lg-2\">\n" +
                                "            <a href=\"/comments?id=" + id + "\"><button class=\"btn btn-large btn-primary\">Comments</button></a>\n" +
                                "        </div>\n" +
                                "        <div>\n" +
                                "            <a href=\"/like?id="+ id + "\"><button class=\"btn btn-large btn-primary\">" + getLikes(id, "likes_post", "postID") + "</button></a>\n" +
                                "        </div>\n" +
                                "        <hr>\n" +
                                "    </div>";
                ///
                String p2 = "</div>\n" +
                        "    <br><br>\n" +
                        "    <div class=\"col-lg-12\"><p>"+POST+"</p></div>\n" +
                        "    <div class=\"col-lg-12\">\n" +
                        "        <p><b>"+author+":</b> <i>"+TIME+"</i></p>\n" +
                        "    </div>\n" +
                        "    <br>\n" +
                        "    <div class=\"col-lg-6\">\n" +
                        "        <div style=\"width: 100px; float: left\">" +
                        "            <a href=\"/comments?id=" + id + "\"><button class=\"btn btn-large btn-primary\">Comments</button></a>\n" +
                        "</div>\n" +
                        "        <div class=\"col-lg-offset-8\" style=\"width: 53px; float: left\"> " +
                        "            <a href=\"/like?id="+ id + "\"><button class=\"btn btn-large btn-primary\">" + getLikes(id, "likes_post", "postID") + "</button></a>\n" +
                        "</div>\n" +
                        "    </div>\n" +
                        "    <hr>\n" +
                        "</div>";
                if (hasPower) {
                    returner += p1 + btn + p2;
                } else returner += p1 + p2;
            } // end while
        } // end try
        catch (SQLException e) {
            e.printStackTrace();
            // Could not connect to the database
        }
        return returner;
    }

    public static boolean addPost(String post, String author, String Date, String title) {
        boolean returner;
        try {
            Connection conc = getConnection();

            String sql = "INSERT INTO posts (title , author, time, postID, post)" +
                    "VALUES (?, ?, ?, ?, ?)";

            PreparedStatement preparedStatement = conc.prepareStatement(sql);
            preparedStatement.setString(1, title);
            preparedStatement.setString(2, author);
            preparedStatement.setString(3, Date);
            preparedStatement.setString(4, String.valueOf(getID("posts")));
            preparedStatement.setString(5, post);
            System.out.println("DONE");
            preparedStatement.executeUpdate();
            returner = true;


        } catch (SQLException e) {
            returner = false;
        }
        return returner;

    }

    static int getID(String table) {
        Connection conc = getConnection();
        int returner;
        try {
            String query = "Select * FROM " + table + ";";
            Statement stmt = conc.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            int max = 1;
            while (resultSet.next()) {
                int temp = resultSet.getInt(1);
                if (max != temp) {
                    break;
                } else max++;
            } // end while
            returner = max;
        } // end try
        catch (SQLException e) {
            e.printStackTrace();
            returner = (int) (Math.random() * 100);
            System.out.println("RANDOM! WATCH OUT!!!");
        }
        return returner;
    } //works!

    public static boolean deletePost(int ID) {
        Connection conc = getConnection();
        boolean returner = false;
        try {
            String query = "DELETE from posts where postID=?;";
            PreparedStatement preparedStatement = conc.prepareStatement(query);
            preparedStatement.setInt(1, ID);
            preparedStatement.executeUpdate();
            returner = true;
        } catch (SQLException e) {
            System.out.println("Error with deleting");
        }
        return returner;

    } //works!

    public static String getComments(int ID, Object comment_author) {
        Connection conc = getConnection();
        String returner = "<h1 style='color: red' >ERROR with DATABASE</h1>";
        try {
            String query = "Select * FROM comments where postID = " + ID;
            Statement stmt = conc.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            returner = "";
            while (resultSet.next()) {
                int id = resultSet.getInt("commentID");
                String author = resultSet.getString("author");
                String part1 =
                        "<div class=\"col-lg-offset-1 col-lg-6\">\n" +
                                "    <div class=\"col-lg-11\"><h3>" + author + ":</h3></div>\n" +
                                "    <div class=\"col-lg-1\" style=\"padding-right: 0px; padding-left: 34px\">";
                String button = "<form method = post action=\"/delete_comment?id=" + id + "\"><button>x</button></form>";

                String part2 = "</div>\n" +
                        "    <div class=\"col-lg-offset-1\"><p>" + resultSet.getString("comment") + "</p></div>\n" +
                        "    <div class=\"col-lg-11\"> <i>" + resultSet.getString("time") + "</i></div>\n" +
                        "    <div class=\"col-lg-1\" style=\"padding-left: 15px\">" +
                        "<form action=\"/like_comment?id=" + id + "\" method = post><button type=\"submit\" class=\"btn btn-large btn-primary\">" +
                        getLikes(id, "likes_comments", "commentID") + "</button></form>" +
                        "</div>\n" +
                        "    <br>\n" +
                        "</div>\n" +
                        "<div class=\"col-lg-12\"><br></div>";
                if (!author.equals(comment_author))
                    returner = part1 + " " + part2;
                else returner += part1 + button + part2;
            }//end while
        } catch (SQLException e) {
        }
        return returner;
    }

    public static boolean likedPost(int ID, String author) {
        Connection conc = getConnection();
        boolean returner = false;
        int id = -1;
        try {
            String query = "Select * FROM likes_post where (postID=" + ID + " AND author like \"" + author + "\")";
            Statement stmt = conc.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            while (resultSet.next()) {
                id = resultSet.getInt(1);
            }
            returner = true;
        } catch (SQLException e) {
            System.out.println("Error with likedPost");
        }
        if (id == -1) {
            System.out.println("Like");
            like(ID, author, "likes_post");
        } else {
            System.out.println("unLike");
            unlike(id, "likes_post", 'p');
        }
        return returner;
    } //works!

    private static void unlike(int ID, String table, char a) {
        Connection conc = getConnection();
        try {
            String query = "DELETE from " + table + " where " + a + "LikeID=?;";
            PreparedStatement preparedStatement = conc.prepareStatement(query);
            preparedStatement.setInt(1, ID);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error with deleting");
        }

    } //works!

    private static void like(int ID, String author, String table) {
        try {
            Connection conc = getConnection();
            String sql = "INSERT INTO " + table +
                    " VALUE (?, ?, ?)";

            PreparedStatement preparedStatement = conc.prepareStatement(sql);
            preparedStatement.setString(1, String.valueOf(getID(table)));
            preparedStatement.setString(2, String.valueOf(ID));
            preparedStatement.setString(3, author);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error with like");
        }
    } //works!

    public static int getLikes(int ID, String table, String column) {
        Connection conc = getConnection();
        int returner = 0;
        try {
            String query = "Select count(*) FROM " + table + " where " + column + "=" + ID;
            Statement stmt = conc.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            while (resultSet.next()) {
                returner = resultSet.getInt(1);
            }
        } catch (SQLException e) {

        }
        return returner;
    } //works!

    public static void addComment(String comment, String author, int id) {
        try {
            Connection conc = getConnection();
            String sql = "INSERT INTO comments (commentID, author, comment, time, postID)" +
                    "VALUES (?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = conc.prepareStatement(sql);
            preparedStatement.setInt(1, getID("comments"));
            preparedStatement.setString(2, author);
            preparedStatement.setString(3, comment);
            preparedStatement.setString(4, String.valueOf(new java.sql.Date((new Date()).getTime())));
            preparedStatement.setInt(5, id);
            System.out.println("added comment");
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Unable to add comment");
            e.printStackTrace();
        }
    }

    public static boolean registrated(String login, String password) {
        int returner = 0;
        Connection conc = getConnection();
        try {
            String query = "Select * FROM users where( login like \"" + login + "\" and password like \"" + password + "\");";
            Statement stmt = conc.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            while (resultSet.next()) {
                resultSet.getString(1);
                returner++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("regisrated error");
        }
        if (returner == 0)
            return false;
        else
            return true;
    }

    public static void registrate(String login, String password, String question, String answer) {
        try {
            Connection conc = getConnection();
            String sql = "INSERT INTO users (login, password, question, answer)" +
                    "VALUES (?, ?, ?, ?)";
            PreparedStatement preparedStatement = conc.prepareStatement(sql);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            preparedStatement.setString(3, question);
            preparedStatement.setString(4, answer);
            preparedStatement.executeUpdate();
            System.out.println("added user");
        } catch (SQLException e) {
            System.out.println("Unable to add user");
            e.printStackTrace();
        }
    }

    public static void close() {
//        try {
//            connection.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
    }

    public static String getQuestion(String login) {
        Connection conc = getConnection();
        String returner = null;
        try {
            String query = "Select question FROM users where login like \"" + login + "\";";
            Statement stmt = conc.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            while (resultSet.next()) {
                returner = resultSet.getString("question");
            }
        } catch (SQLException e) {
            System.out.println("Error with getting question");
        }
        return returner;
    }

    public static String isTrue(String login, String answer) {
        Connection conc = getConnection();
        String returner = null;
        try {
            String query = "Select password FROM users where (login like \"" + login + "\" and answer like \"" + answer + "\");";
            Statement stmt = conc.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            while (resultSet.next()) {
                returner = resultSet.getString("password");
            }
        } catch (SQLException e) {
            System.out.println("Error with getting password");
        }
        return returner;
    }

    public static boolean deleteComment(int ID) {
        Connection conc = getConnection();
        boolean returner = false;
        try {
            String query = "DELETE from comments where commentID=?;";
            PreparedStatement preparedStatement = conc.prepareStatement(query);
            preparedStatement.setInt(1, ID);
            preparedStatement.executeUpdate();
            returner = true;
        } catch (SQLException e) {
            System.out.println("Error with deleting comment");
        }
        return returner;

    }

    public static boolean likedComment(int ID, String author) {
        Connection conc = getConnection();
        boolean returner = false;
        int id = -1;
        try {
            String query = "Select * FROM likes_comments where (commentID=" + ID + " AND author like \"" + author + "\")";
            Statement stmt = conc.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            while (resultSet.next()) {
                id = resultSet.getInt(1);
            }
            returner = true;
        } catch (SQLException e) {
            System.out.println("Error with likedComment");
        }
        if (id == -1) {
            System.out.println("Like");
            like(ID, author, "likes_comments");
        } else {
            System.out.println("unLike");
            unlike(id, "likes_comments", 'c');
        }
        return returner;
    }

}