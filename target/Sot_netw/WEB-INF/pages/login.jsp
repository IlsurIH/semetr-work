<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <META content="text/html" charset="utf-8" http-equiv="Content-Type">
    <title>Вход</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        body {
            padding-top: 40px;
            padding-bottom: 40px;
            background-image: url("http://hq-wallpapers.ru/wallpapers/13/hq-wallpapers_ru_nature_61651_1920x1200.jpg");
        }
        button{
            left:730;
        }

        .form-signin {
            max-width: 300px;
            padding: 19px 29px 29px;
            margin: 0 auto 20px;
            background-color: #fff;
            border: 1px solid #e5e5e5;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
        }
        .form-signin input[type="text"],
        .form-signin input[type="password"] {
            font-size: 16px;
            height: auto;
            margin-bottom: 15px;
            padding: 7px 9px;
        }

    </style>

</head>
<body>
<form action ="login" method="post" class="form-signin">
    <h3 for=login>Логин</h3>
    <input id=login  name=login tabindex="1" type=text value = "true" autofocus>
    <h3 for=password>Пароль</h3>
    <input id=password tabindex="2" name=password value="true" type="password"/>
    ${login_error}
    <br>
    <button class="btn btn-large btn-primary" tabindex="3" type="submit">Войти</button>

    <br>
    <a tabindex="4" href="forgot">Забыли пароль?</a>
    <hr>
    <a  class="btn btn-large btn-primary" tabindex="5" href="registration" >Присоединиться</a>
</form>
<hr>
<br>
</body>
</html>
